<?php
  /*
   *  Add setting for overlaying the header
   */
  function rkt_add_overlay_header( $page ) {
    $value = esc_html( get_post_meta( $page->ID, 'rkt_header_position', true ) ); ?>
    <table>
      <tr>
        <td>
          <span style="margin-right: 15px;"><?php echo esc_html__( 'Overlay header?', 'rkt-shortcodes-general' ) ?></span>
          <input type="checkbox" name="rkt_header_position_flag" id="rkt_header_position_flag" value="yes" <?php if ( isset ( $value ) ) checked( $value, 'yes' ); ?> />
          <br/>
          <p style="font-style: oblique; color: #777;">
            <?php echo esc_html__( 'If checked the header (logo and nav) will overlay itself onto the top most content. Useful for overlaying onto sliders or hero images.', 'rkt-shortcodes-general' ) ?>
          </p>
        </td>
      </tr>
    </table>
    <?php
  }

  // Saving the data
  add_action( 'save_post', 'rkt_add_header_position_fields', 10, 2 );
  function rkt_add_header_position_fields( $page_id, $page ) {
    if ( $page->post_type == 'page' ) {
      if( isset( $_POST[ 'rkt_header_position_flag' ] ) ) {
        update_post_meta( $page_id, 'rkt_header_position', 'yes' );
      } else {
        update_post_meta( $page_id, 'rkt_header_position', 'no' );
      }
    }
  }

  // Use following line in for example header.php to read above option and display a class
  // echo (  get_post_meta( get_the_ID(), 'rkt_header_position', true ) === 'yes' ) ? 'overlay ' : '';
?>
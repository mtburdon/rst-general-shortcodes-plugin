<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'label'  => array(
		'label' => esc_html__( 'Button Label', 'rkt-shortcodes-general' ),
		'desc'  => esc_html__( 'This is the text that appears on your button', 'rkt-shortcodes-general' ),
		'type'  => 'text',
		'value' => esc_html__( 'Submit', 'rkt-shortcodes-general' )
	),
	'link'   => array(
		'label' => esc_html__( 'Button Link', 'rkt-shortcodes-general' ),
		'desc'  => esc_html__( 'Where should your button link to', 'rkt-shortcodes-general' ),
		'type'  => 'text',
		'value' => '#'
	),
	'target' => array(
		'type'  => 'switch',
		'label'   => esc_html__( 'Open Link in New Window', 'rkt-shortcodes-general' ),
		'desc'    => esc_html__( 'Select here if you want to open the linked page in a new window', 'rkt-shortcodes-general' ),
		'value'  => '_self',
		'right-choice' => array(
			'value' => '_blank',
			'label' => esc_html__('Yes', 'rkt-shortcodes-general'),
		),
		'left-choice' => array(
			'value' => '_self',
			'label' => esc_html__('No', 'rkt-shortcodes-general'),
		),
	),
	'color'  => array(
		'label'   => esc_html__( 'Button Color', 'rkt-shortcodes-general' ),
		'desc'    => esc_html__( 'Choose a color for your button', 'rkt-shortcodes-general' ),
		'type'    => 'select',
		'value'  => 'accent-1',
		'choices' => array(
			'accent-1' => esc_html__('Accent 1', 'rkt-shortcodes-general'),
			'accent-2' => esc_html__('Accent 2', 'rkt-shortcodes-general'),
			'dark' => esc_html__( 'Dark', 'rkt-shortcodes-general' ),
			'light' => esc_html__( 'Light', 'rkt-shortcodes-general' )
		)
	),
	'fill_style' => array(
		'label'   => esc_html__( 'Fill style', 'rkt-shortcodes-general' ),
		'desc'    => esc_html__( 'Should it appear as an outline button or a fill?', 'rkt-shortcodes-general' ),
		'type'  => 'radio',
		'value'  => 'filled',
		'choices' => array(
			'filled' => esc_html__('Filled', 'rkt-shortcodes-general'),
			'outline' => esc_html__('Outline', 'rkt-shortcodes-general')
		)
	),
	'size' => array(
		'label' => esc_html__( 'Button Size', 'rkt-shortcodes-general' ),
		'desc' => esc_html__( 'Choose a size for your button', 'rkt-shortcodes-general' ),
		'type' => 'radio',
		'value'  => 'medium',
		'choices' => array(
			'small' => esc_html__('Small', 'rkt-shortcodes-general'),
			'medium' => esc_html__('Medium', 'rkt-shortcodes-general'),
			'large' => esc_html__( 'Large', 'rkt-shortcodes-general' )
		)
	),
	'button_width' => array(
		'label'   => esc_html__( 'Button width', 'rkt-shortcodes-general' ),
		'desc'    => esc_html__( 'Should the button be full width or normal?', 'rkt-shortcodes-general' ),
		'type'  => 'radio',
		'value'  => 'normal',
		'choices' => array(
			'normal' => esc_html__('Normal', 'rkt-shortcodes-general'),
			'full_width' => esc_html__('Full Width', 'rkt-shortcodes-general')
		)
	),
	'shape' => array(
		'label'   => esc_html__( 'Button shape', 'rkt-shortcodes-general' ),
		'desc'    => esc_html__( 'Select the button shape', 'rkt-shortcodes-general' ),
		'type'  => 'radio',
		'value'  => 'round',
		'choices' => array(
			'round' => esc_html__('Round', 'rkt-shortcodes-general'),
			'square' => esc_html__('Square', 'rkt-shortcodes-general'),
			'pill' => esc_html__( 'Pill', 'rkt-shortcodes-general' )
		)
	)
);

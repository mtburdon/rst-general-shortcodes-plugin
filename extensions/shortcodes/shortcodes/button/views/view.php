<?php
	if (!defined('FW')) die( 'Forbidden' );

	//-------------------------------
	// @label
	// @link
	// @target
	// @color
	// @fill_style
	// @size
	// @button_width
	// @shape
	//-------------------------------

	$button_classes = '';

	// Color (accent-1 / accent-2 / dark / light)
	$button_classes .= " {$atts['color']}";

	// Fill style (outline / filled)
	$button_classes .= " {$atts['fill_style']}";

	// Size (small / medium / large)
	$button_classes .= " {$atts['size']}";

	// Width
	if ( $atts['button_width'] == 'full_width' ) {
		$button_classes .= " full-width";
	}

	// Shape
	$button_classes .= " {$atts['shape']}";
?>

<a href="<?php echo esc_url($atts['link']) ?>" target="<?php echo esc_attr($atts['target']) ?>" class="btn <?php echo esc_attr($button_classes) ?>">
	<?php echo esc_html($atts['label']); ?>
</a>

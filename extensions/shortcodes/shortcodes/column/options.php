<?php if (!defined('FW')) {
  die('Forbidden');
}

$options = array(
  'col_class' => array(
    'label' => esc_html__('Column Class', 'rkt-shortcodes-general'),
    'desc'  => esc_html__('Give this column a class if you wish to add some custom CSS', 'rkt-shortcodes-general'),
    'help'  => esc_html__('If you would like to apply some custom CSS to this column add a class then use it within the Custom CSS tab in the Theme Settings', 'rkt-shortcodes-general'),
    'type'  => 'text',
    'attr'  => array( 'class' => 'code-field' )
  ),
  'remove_col_padding' => array(
    'label' => esc_html__('Remove spacing', 'rkt-shortcodes-general'),
    'desc'  => esc_html__('Remove default left and right padding from this column', 'rkt-shortcodes-general'),
    'type'  => 'switch',
  ),
  'custom_padding' => array(
    'type' => 'multi-picker',
    'label' => false,
    'desc' => false,
    'picker' => array(
      'padding_switch' => array(
        'label' => esc_html__( 'Custom padding', 'rkt-shortcodes-general' ),
        'desc' => esc_html__( 'Set up some custom padding on this column, overriding the default padding', 'rkt-shortcodes-general' ),
        'type' => 'switch',
        'right-choice' => array(
          'value' => 'yes',
          'label' => esc_html__( 'yes', 'rkt-shortcodes-general' )
        ),
        'left-choice'  => array(
          'value' => 'no',
          'label' => esc_html__( 'No', 'rkt-shortcodes-general' )
        ),
        'value' => 'no',
      )
    ),
    'choices' => array(
      'yes' => array(
        'html_label'     => array(
          'type'  => 'html',
          'value' => '',
          'label' => esc_html__( 'Padding', 'rkt-shortcodes-general' ),
          'html'  => '',
        ),
        'padding_top' => array(
          'label' => false,
          'desc'  => esc_html__('Top', 'rkt-shortcodes-general'),
          'type'  => 'short-text'
        ),
        'padding_right' => array(
          'label' => false,
          'desc'  => esc_html__('Right', 'rkt-shortcodes-general'),
          'type'  => 'short-text'
        ),
        'padding_bottom' => array(
          'label' => false,
          'desc'  => esc_html__('Bottom', 'rkt-shortcodes-general'),
          'type'  => 'short-text'
        ),
        'padding_left' => array(
          'label' => false,
          'desc'  => esc_html__('Left', 'rkt-shortcodes-general'),
          'type'  => 'short-text'
        )
      ),
      'no' => array(),
    ),
    'show_borders' => false,
  ),
  'col_background_settings' => array(
    'type'         => 'multi-picker',
    'label'        => false,
    'desc'         => false,
    'picker'       => array(
      'background' => array(
        'label'   => esc_html__( 'Background Settings', 'rkt-shortcodes-general' ),
        'desc'    => esc_html__( 'Set up some background settings for this column', 'rkt-shortcodes-general' ),
        'type'    => 'radio',
        'choices' => array(
          'none'    => esc_html__( 'None', 'rkt-shortcodes-general' ),
          'image'   => esc_html__( 'Image', 'rkt-shortcodes-general' ),
          'colour'   => esc_html__( 'Colour', 'rkt-shortcodes-general' )
        ),
        'value'   => 'none'
      ),
    ),
    'choices' => array(
      'none'  => array(),
      'image' => array(
        'background_image' => array(
          'label'   => '',
          'type'    => 'background-image',
          'choices' => array(
          )
        ),
        'parallax' => array(
          'type'    => 'multi-picker',
          'label'   => false,
          'desc'    => false,
          'picker'  => array(
            'selected' => array(
              'type'         => 'switch',
              'label'        => esc_html__( 'Parallax Effect', 'rkt-shortcodes-general' ),
              'desc'         => esc_html__( 'Create a parallax effect on scroll?', 'rkt-shortcodes-general' ),
              'help'         => esc_html__( 'For best results use an image that is taller than the column\'s height.', 'rkt-shortcodes-general' ),
              'value'        => '',
              'right-choice' => array(
                'value' => 'yes',
                'label' => esc_html__( 'Yes', 'rkt-shortcodes-general' ),
              ),
              'left-choice'  => array(
                'value' => 'no',
                'label' => esc_html__( 'No', 'rkt-shortcodes-general' ),
              ),
            ),
          ),
          'choices' => array(
            'yes' => array(
              'parallax_speed' => array(
                'label'      => '',
                'desc'       => esc_html__( 'Set the parallax speed', 'rkt-shortcodes-general' ),
                'type'       => 'slider',
                'value'      => 5,
                'properties' => array(
                  'min' => 1,
                  'max' => 10,
                  'sep' => 1,
                  'grid_snap' => true
                ),
              ),
            )
          )
        )
      ),
      'colour' => array(
        'background_colour' => array(
          'label'   => '',
          'help'    => esc_html__( 'Set a background colour for this column', 'rkt-shortcodes-general' ),
          'desc'    => esc_html__( 'Select the background color', 'rkt-shortcodes-general' ),
          'value'   => '',
          'type'    => 'rgba-color-picker'
        ),
      ),
    ),
    'show_borders' => false,
  ),
  'animation_settings' => array(
    'type' => 'multi-picker',
    'label'   => false,
    'desc'    => false,
    'picker'  => array(
      'selected' => array(
        'type'  => 'switch',
        'label' => esc_html__( 'Animation', 'rkt-shortcodes-general' ),
        'help'  => esc_html__( 'Add an animation in for this element. Animation demo\'s: <a target="_blank" href="http://daneden.github.io/animate.css/">here</a>.', 'rkt-shortcodes-general' ),
        'value' => 'no',
        'right-choice' => array(
          'value' => 'yes',
          'label' => esc_html__( 'Yes', 'rkt-shortcodes-general' ),
        ),
        'left-choice'  => array(
          'value' => 'no',
          'label' => esc_html__( 'No', 'rkt-shortcodes-general' ),
        ),
      ),
    ),
    'choices' => array(
      'yes' => array(
        'animation_delay' => array(
          'type' => 'short-text',
          'label' => esc_html__('Animation Delay', 'rkt-shortcodes-general'),
          'desc'  => esc_html__('Add a delay in milliseconds before starting the animation', 'rkt-shortcodes-general'),
          'value' => '200'
        ),
        'animation_type' => array(
          'type'  => 'select',
          'label' => esc_html__('Animation Type', 'rkt-shortcodes-general'),
          'desc'  => esc_html__('Select an animation type', 'rkt-shortcodes-general'),
          'value' => 'fadeInUp',
          'choices' => array(
            array(
              'attr'  => array('label' => esc_html__('Attention Seekers', 'rkt-shortcodes-general')),
              'choices' => array(
                  'bounce' => esc_html__('Bounce', 'rkt-shortcodes-general'),
                  'flash' => esc_html__('Flash', 'rkt-shortcodes-general'),
                  'pulse' => esc_html__('Pulse', 'rkt-shortcodes-general'),
                  'rubberBand' => esc_html__('Rubber Band', 'rkt-shortcodes-general'),
                  'shake' => esc_html__('Shake', 'rkt-shortcodes-general'),
                  'swing' => esc_html__('Swing', 'rkt-shortcodes-general'),
                  'tada' => esc_html__('Tada', 'rkt-shortcodes-general'),
                  'wobble' => esc_html__('Wobble', 'rkt-shortcodes-general'),
                  'jello' => esc_html__('Jello', 'rkt-shortcodes-general')
              ),
            ),
            array(
              'attr'  => array('label' => esc_html__('Bouncing Entrances', 'rkt-shortcodes-general')),
              'choices' => array(
                  'bounceIn' => esc_html__('Bounce In', 'rkt-shortcodes-general'),
                  'bounceInDown' => esc_html__('Bounce In Down', 'rkt-shortcodes-general'),
                  'bounceInLeft' => esc_html__('Bounce In Left', 'rkt-shortcodes-general'),
                  'bounceInRight' => esc_html__('Bounce In Right', 'rkt-shortcodes-general'),
                  'bounceInUp' => esc_html__('Bounce In Up', 'rkt-shortcodes-general'),
              ),
            ),
            array(
              'attr'  => array('label' => esc_html__('Bouncing Exits', 'rkt-shortcodes-general')),
              'choices' => array(
                  'bounceOut' => esc_html__('Bounce Out', 'rkt-shortcodes-general'),
                  'bounceOutDown' => esc_html__('Bounce Out Down', 'rkt-shortcodes-general'),
                  'bounceOutLeft' => esc_html__('Bounce Out Left', 'rkt-shortcodes-general'),
                  'bounceOutRight' => esc_html__('Bounce Out Right', 'rkt-shortcodes-general'),
                  'bounceOutUp' => esc_html__('Bounce Out Up', 'rkt-shortcodes-general'),
              ),
            ),
            array(
              'attr'  => array('label' => esc_html__('Fading Entrances', 'rkt-shortcodes-general')),
              'choices' => array(
                  'fadeIn' => esc_html__('Fade In', 'rkt-shortcodes-general'),
                  'fadeInDown' => esc_html__('Fade In Down', 'rkt-shortcodes-general'),
                  'fadeInDownBig' => esc_html__('Fade In Down Big', 'rkt-shortcodes-general'),
                  'fadeInLeft' => esc_html__('Fade In Left', 'rkt-shortcodes-general'),
                  'fadeInLeftBig' => esc_html__('Fade In Left Big', 'rkt-shortcodes-general'),
                  'fadeInRight' => esc_html__('Fade In Right', 'rkt-shortcodes-general'),
                  'fadeInRightBig' => esc_html__('Fade In Right Big', 'rkt-shortcodes-general'),
                  'fadeInUp' => esc_html__('Fade In Up', 'rkt-shortcodes-general'),
                  'fadeInUpBig' => esc_html__('Fade In Up Big', 'rkt-shortcodes-general')
              ),
            ),
            array(
              'attr'  => array('label' => esc_html__('Fading Exits', 'rkt-shortcodes-general')),
              'choices' => array(
                  'fadeOut' => esc_html__('Fade Out', 'rkt-shortcodes-general'),
                  'fadeOutDown' => esc_html__('Fade Out Down', 'rkt-shortcodes-general'),
                  'fadeOutDownBig' => esc_html__('Fade Out Down Big', 'rkt-shortcodes-general'),
                  'fadeOutLeft' => esc_html__('Fade Out Left', 'rkt-shortcodes-general'),
                  'fadeOutLeftBig' => esc_html__('Fade Out Left Big', 'rkt-shortcodes-general'),
                  'fadeOutRight' => esc_html__('Fade Out Right', 'rkt-shortcodes-general'),
                  'fadeOutRightBig' => esc_html__('Fade Out Right Big', 'rkt-shortcodes-general'),
                  'fadeOutUp' => esc_html__('Fade Out Up', 'rkt-shortcodes-general'),
                  'fadeOutUpBig' => esc_html__('Fade Out Up Big', 'rkt-shortcodes-general')
              ),
            ),
            array(
              'attr'  => array('label' => esc_html__('Flippers', 'rkt-shortcodes-general')),
              'choices' => array(
                'flip' => esc_html__('Flip', 'rkt-shortcodes-general'),
                'flipInX' => esc_html__('Flip In X', 'rkt-shortcodes-general'),
                'flipInY' => esc_html__('Flip In Y', 'rkt-shortcodes-general'),
                'flipOutX' => esc_html__('Flip Out X', 'rkt-shortcodes-general'),
                'flipOutY' => esc_html__('Flip Out Y', 'rkt-shortcodes-general')
              ),
            ),
            array(
              'attr'  => array('label' => esc_html__('Lightspeed', 'rkt-shortcodes-general')),
              'choices' => array(
                'lightSpeedIn' => esc_html__('Light Speed In', 'rkt-shortcodes-general'),
                'lightSpeedOut' => esc_html__('Light Speed Out', 'rkt-shortcodes-general')
              ),
            ),
            array(
              'attr'  => array('label' => esc_html__('Rotating Entrances', 'rkt-shortcodes-general')),
              'choices' => array(
                'rotateIn' => esc_html__('Rotate In', 'rkt-shortcodes-general'),
                'rotateInDownLeft' => esc_html__('Rotate In Down Left', 'rkt-shortcodes-general'),
                'rotateInDownRight' => esc_html__('Rotate In Down Right', 'rkt-shortcodes-general'),
                'rotateInUpLeft' => esc_html__('Rotate In Up Left', 'rkt-shortcodes-general'),
                'rotateInUpRight' => esc_html__('Rotate In Up Right', 'rkt-shortcodes-general')
              ),
            ),
            array(
              'attr'  => array('label' => esc_html__('Rotating Exits', 'rkt-shortcodes-general')),
              'choices' => array(
                'rotateOut' => esc_html__('Rotate Out', 'rkt-shortcodes-general'),
                'rotateOutDownLeft' => esc_html__('Rotate Out Down Left', 'rkt-shortcodes-general'),
                'rotateOutDownRight' => esc_html__('Rotate Out Down Right', 'rkt-shortcodes-general'),
                'rotateOutUpLeft' => esc_html__('Rotate Out Up Left', 'rkt-shortcodes-general'),
                'rotateOutUpRight' => esc_html__('Rotate Out Up Right', 'rkt-shortcodes-general')
              ),
            ),
            array(
              'attr'  => array('label' => esc_html__('Sliding Entrances', 'rkt-shortcodes-general')),
              'choices' => array(
                'slideInUp' => esc_html__('Slide In Up', 'rkt-shortcodes-general'),
                'slideInDown' => esc_html__('Slide In Down', 'rkt-shortcodes-general'),
                'slideInLeft' => esc_html__('Slide In Left', 'rkt-shortcodes-general'),
                'slideInRight' => esc_html__('Slide In Right', 'rkt-shortcodes-general')
              ),
            ),
            array(
              'attr'  => array('label' => esc_html__('Sliding Exits', 'rkt-shortcodes-general')),
              'choices' => array(
                'slideOutUp' => esc_html__('Slide Out Up', 'rkt-shortcodes-general'),
                'slideOutDown' => esc_html__('Slide Out Down', 'rkt-shortcodes-general'),
                'slideOutLeft' => esc_html__('Slide Out Left', 'rkt-shortcodes-general'),
                'slideOutRight' => esc_html__('Slide Out Right', 'rkt-shortcodes-general')
              ),
            ),
            array(
              'attr'  => array('label' => esc_html__('Zoom Entrances', 'rkt-shortcodes-general')),
              'choices' => array(
                'zoomIn' => esc_html__('Zoom In', 'rkt-shortcodes-general'),
                'zoomInDown' => esc_html__('Zoom In Down', 'rkt-shortcodes-general'),
                'zoomInLeft' => esc_html__('Zoom In Left', 'rkt-shortcodes-general'),
                'zoomInRight' => esc_html__('Zoom In Right', 'rkt-shortcodes-general'),
                'zoomInUp' => esc_html__('Zoom In Up', 'rkt-shortcodes-general')
              ),
            ),
            array(
              'attr'  => array('label' => esc_html__('Zoom Exits', 'rkt-shortcodes-general')),
              'choices' => array(
                'zoomOut' => esc_html__('Zoom Out', 'rkt-shortcodes-general'),
                'zoomOutDown' => esc_html__('Zoom Out Down', 'rkt-shortcodes-general'),
                'zoomOutLeft' => esc_html__('Zoom Out Left', 'rkt-shortcodes-general'),
                'zoomOutRight' => esc_html__('Zoom Out Right', 'rkt-shortcodes-general'),
                'zoomOutUp' => esc_html__('Zoom Out Up', 'rkt-shortcodes-general')
              ),
            ),
            array(
              'attr'  => array('label' => esc_html__('Specials', 'rkt-shortcodes-general')),
              'choices' => array(
                'hinge' => esc_html__('Hinge', 'rkt-shortcodes-general'),
                'rollIn' => esc_html__('Roll In', 'rkt-shortcodes-general'),
                'rollOut' => esc_html__('Roll Out', 'rkt-shortcodes-general')
              ),
            )
          )
        )
      ),
    ),
  )
);
<?php if (!defined('FW')) die('Forbidden');

$column_class = fw_ext_builder_get_item_width('page-builder', $atts['width'] . '/frontend_class');
$col_extra_styles = '';

if ( isset($atts['col_class']) ) {
  $column_class .= ' ' . $atts['col_class'];
}

if ( isset($atts['remove_col_padding']) && $atts['remove_col_padding']) {
  $col_extra_styles .= ' padding: 0;';
}

/*
 *  Padding
 */
if ($atts['custom_padding']['padding_switch'] === 'yes') {
  $atts_padding = $atts['custom_padding']['yes'];

  if ((int)$atts_padding['padding_top']) {
    $col_extra_styles .= ' padding-top:' . (int)$atts_padding['padding_top'] . 'px;';
  }

  if ((int)$atts_padding['padding_right']) {
    $col_extra_styles .= ' padding-right:' . (int)$atts_padding['padding_right'] . 'px;';
  }

  if ((int)$atts_padding['padding_bottom']) {
    $col_extra_styles .= ' padding-bottom:' . (int)$atts_padding['padding_bottom'] . 'px;';
  }

  if ((int)$atts_padding['padding_left']) {
    $col_extra_styles .= ' padding-left:' . (int)$atts_padding['padding_left'] . 'px;';
  }
}

/*
 *  Col background
 */
/*
 *  Main section background options
 */
// BG image
$col_parallax = '';
if ( $atts['col_background_settings']['background'] == 'image' && !empty( $atts['col_background_settings']['image']['background_image']['data'] ) ) {
  $col_extra_styles .= ' background-image:url(' . $atts['col_background_settings']['image']['background_image']['data']['icon'] . ');';

  // Parallax
  if ( $atts['col_background_settings']['image']['parallax']['selected'] == 'yes' ) {
    $col_parallax = 'data-stellar-background-ratio=' . ((int)$atts['col_background_settings']['image']['parallax']['yes']['parallax_speed'] / 10) . '';
    $column_class .= ' parallax';
  }
}

// BG colour
if ( $atts['col_background_settings']['background'] == 'colour' && isset($atts['col_background_settings']['colour']['background_colour']) && $atts['col_background_settings']['colour']['background_colour'] != '') {
  $col_extra_styles .= ' background-color: ' . $atts['col_background_settings']['colour']['background_colour'] . ';';
}

/*
 *  Animations
 */
$col_animation = '';
if ( isset( $atts['animation_settings']['selected'] ) && $atts['animation_settings']['selected'] == 'yes' ) {
  $column_class .= ' animated-el';

  if ( ! empty( $atts['animation_settings']['yes']['animation_type'] ) ) {
    $col_animation = 'data-animation-type=' . $atts['animation_settings']['yes']['animation_type'] . '';
  }

  if ( ! empty( $atts['animation_settings']['yes']['animation_delay'] ) ) {
    $col_animation .= ' data-animation-delay=' . (int)$atts['animation_settings']['yes']['animation_delay'] . '';
  }
}
?>
<div class="<?php echo esc_attr($column_class); ?>" style="<?php echo esc_attr($col_extra_styles); ?>" <?php echo esc_attr($col_parallax); ?> <?php echo esc_attr($col_animation); ?>>
  <?php echo do_shortcode($content); ?>
</div>

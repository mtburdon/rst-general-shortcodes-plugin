<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
	'page_builder' => array(
		'title'       => esc_html__( 'Countdown', 'rkt-shortcodes-general' ),
		'description' => esc_html__( 'Add a countdown to the big day', 'rkt-shortcodes-general'),
		'tab'         => esc_html__( 'Rocketship Themes', 'rkt-shortcodes-general' )
	)
);
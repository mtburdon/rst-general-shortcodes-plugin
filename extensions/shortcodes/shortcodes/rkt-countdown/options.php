<?php if (!defined('FW')) die('Forbidden');

$options = array(
	'datetime' => array(
		'label'   => esc_html__('Event Date/Time', 'rkt-shortcodes-general'),
		'desc'    => esc_html__('Select the date and time of the event', 'rkt-shortcodes-general'),
		'type'    => 'datetime-picker',
		'min-date' => null,
		'datetime-picker' => array(
			'step' => 15
		)
	),
	'number_colour' => array(
		'type'  => 'rgba-color-picker',
		'value' => 'rgba(25, 25, 25, 1)',
		'label' => esc_html__('Number Colour', 'rkt-shortcodes-general'),
		'desc'  => esc_html__('Set the colour and opacity of the number text', 'rkt-shortcodes-general')
	),
	'text_colour' => array(
		'type'  => 'rgba-color-picker',
		'value' => 'rgba(25, 25, 25, 1)',
		'label' => esc_html__('Text Colour', 'rkt-shortcodes-general'),
		'desc'  => esc_html__('Set the colour and opacity of the text', 'rkt-shortcodes-general')
	)
);
<?php if (!defined('FW')) die('Forbidden');

$uri = plugin_dir_url( __FILE__ );

wp_enqueue_script(
	'fw-countdown-plugin-js',
	$uri . '/static/js/jquery.plugin.min.js',
	array('jquery'),
	'1.0.1',
	true
);

wp_enqueue_script(
	'fw-countdown-js',
	$uri . '/static/js/jquery.countdown.min.js',
	array('jquery', 'fw-countdown-plugin-js'),
	'2.0.1',
	true
);
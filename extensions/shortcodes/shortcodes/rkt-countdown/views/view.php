<?php if (!defined('FW')) die('Forbidden');

// Get a unique ID so we can show multiple forms
$uid = "id_" . base_convert(microtime(), 10, 36);
$datetime = $atts['datetime'];
$number_colour = $atts['number_colour'];
$text_colour = $atts['text_colour'];
?>
<script>
	jQuery(function() {
		var datetime = '<?php echo esc_js($datetime) ?>';
		datetime = datetime.split(' ');
		var date = datetime[0].split('/');
		var time = datetime[1].split(':');
		var day = date[2];
		var month = date[1] - 1;
		var year = date[0];
		var hour = time[0];
		var mins = time[1];
		var myDate = new Date(year, month, day, hour, mins);
		jQuery("#<?php echo sanitize_html_class($uid) ?>").countdown({ until: myDate });
	});
</script>
<style>
	#<?php echo sanitize_html_class($uid); ?> .countdown-amount {
		color: <?php echo esc_attr($number_colour) ?>;
	}

	#<?php echo sanitize_html_class($uid); ?> .countdown-period {
		color: <?php echo esc_attr($text_colour) ?>;
	}
</style>
<div class="rkt-countdown">
	<div id="<?php echo sanitize_html_class($uid) ?>" class="countdown-container"></div>
</div>
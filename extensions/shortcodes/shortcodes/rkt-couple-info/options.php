<?php if (!defined('FW')) die('Forbidden');

$options = array(
	'name' => array(
		'label'   => esc_html__('Name', 'rkt-shortcodes-general'),
		'desc'    => esc_html__('Add the name of this person', 'rkt-shortcodes-general'),
		'type'    => 'text'
	),
	'image' => array(
		'label'   => esc_html__('Image', 'rkt-shortcodes-general'),
		'desc'    => esc_html__('Add an image of this person', 'rkt-shortcodes-general'),
		'type'    => 'upload',
		'images_only' => true
	),
	'description' => array(
		'label' => esc_html__('Description', 'rkt-shortcodes-general'),
		'type' => 'textarea',
	),
	'twitter' => array(
		'label'   => esc_html__('Twitter', 'rkt-shortcodes-general'),
		'desc'    => esc_html__('Add the link to this persons Twitter account', 'rkt-shortcodes-general'),
		'type'    => 'text'
	),
	'facebook' => array(
		'label'   => esc_html__('Facebook', 'rkt-shortcodes-general'),
		'desc'    => esc_html__('Add the link to this persons Facebook account', 'rkt-shortcodes-general'),
		'type'    => 'text'
	),
	'instagram' => array(
		'label'   => esc_html__('Instagram', 'rkt-shortcodes-general'),
		'desc'    => esc_html__('Add the link to this persons Instagram account', 'rkt-shortcodes-general'),
		'type'    => 'text'
	)
);
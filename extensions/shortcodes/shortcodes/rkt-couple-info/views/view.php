<?php if (!defined('FW')) die('Forbidden');

$name = $atts['name'];
$image = '';
$alt_text = '';
if (!empty($atts['image'])) {
	$image = $atts['image']['url'];
	$alt_text = get_post_meta($atts['image']['attachment_id'], '_wp_attachment_image_alt', true);
}

$twitter = $atts['twitter'];
$facebook = $atts['facebook'];
$instagram = $atts['instagram'];
?>

<div class="rkt-couple-info">
	<img class="headshot" alt="<?php echo esc_html($alt_text) ?>" src="<?php echo esc_url($image); ?>">
	<div class="couple-inner">
		<h3 class="name"><?php echo esc_html($name); ?></h3>
		<div class="heading-divider"></div>
		<?php if ($atts['description']) { ?><p class="description"><?php echo esc_html($atts['description']); ?></p><?php } ?>
		<?php if ($twitter !== '' || $facebook !== '' || $instagram !== '') { ?>
			<div class="social">
				<?php if ($twitter !== '') { ?><a href="<?php echo esc_url($twitter); ?>" target="_blank"><i class="fa fa-twitter-square"></i></a><?php } ?>
				<?php if ($facebook !== '') { ?><a href="<?php echo esc_url($facebook); ?>" target="_blank"><i class="fa fa-facebook-square"></i></a><?php } ?>
				<?php if ($instagram !== '') { ?><a href="<?php echo esc_url($instagram); ?>" target="_blank"><i class="fa fa-instagram"></i></a><?php } ?>
			</div>
		<?php } ?>
	</div>
</div>
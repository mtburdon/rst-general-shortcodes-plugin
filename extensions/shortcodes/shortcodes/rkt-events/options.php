<?php if (!defined('FW')) die('Forbidden');

$options = array(
	'title' => array(
		'label'   => esc_html__('Event title', 'rkt-shortcodes-general'),
		'type'    => 'text',
		'desc'    => esc_html__('Add the title of the event', 'rkt-shortcodes-general'),
		'value'		=> 'Wedding Ceremony'
	),
	'datetime' => array(
		'label'   => esc_html__('Event Date/Time', 'rkt-shortcodes-general'),
		'type'    => 'datetime-picker',
		'desc'    => esc_html__('Select the date and time of the event', 'rkt-shortcodes-general'),
		'min-date' => null,
		'datetime-picker' => array(
			'step' => 15
		)
	),
  'hide_date_time' => array(
    'type'  => 'checkboxes',
    'value' => array(
      'choice-1' => false,
      'choice-2' => false
    ),
    'label' => __('Hide date or time', 'rkt-shortcodes-general'),
    'desc'  => __('Choose to hide date, time or both', 'rkt-shortcodes-general'),
    'choices' => array( // Note: Avoid bool or int keys http://bit.ly/1cQgVzk
      'choice-1' => __('Date', 'rkt-shortcodes-general'),
      'choice-2' => __('Time', 'rkt-shortcodes-general')
    ),
    'inline' => true,
  ),
	'date_format' => array(
		'label'   => esc_html__('Date format', 'rkt-shortcodes-general'),
		'type'    => 'select',
		'desc'    => esc_html__('Select how to display the date', 'rkt-shortcodes-general'),
		'choices' => array(
			'jS M Y' => esc_html__('2nd Aug 2013', 'rkt-shortcodes-general'),
			'jS F Y' => esc_html__('2nd August 2013', 'rkt-shortcodes-general'),
			'd.m.Y' => esc_html__('02.08.2013', 'rkt-shortcodes-general'),
			'd/m/Y' => esc_html__('02/08/2013', 'rkt-shortcodes-general')
		)
	),
	'time_format' => array(
		'label'   => esc_html__('Time format', 'rkt-shortcodes-general'),
		'type'    => 'select',
		'desc'    => esc_html__('Select how to display the time', 'rkt-shortcodes-general'),
		'choices' => array(
			'h:i A' => esc_html__('02:00 PM', 'rkt-shortcodes-general'),
			'H\hi' => esc_html__('14h00', 'rkt-shortcodes-general'),
			'H:i' => esc_html__('14:00', 'rkt-shortcodes-general'),
		)
	),
	'description' => array(
		'label'   => esc_html__('Event Description', 'rkt-shortcodes-general'),
		'type'    => 'textarea',
		'desc'    => esc_html__('Add the details for the event', 'rkt-shortcodes-general')
	),
	'location' => array(
		'label'   => esc_html__('Event Location', 'rkt-shortcodes-general'),
		'type'    => 'textarea',
		'desc'    => esc_html__('Add the location for the event', 'rkt-shortcodes-general')
	),
	'icon_image' => array(
		'label'   => esc_html__('Icon', 'rkt-shortcodes-general'),
		'type'    => 'wp-editor',
		'desc'    => esc_html__('Insert some SVG code (recommended) or an img tag with a link to an icon', 'rkt-shortcodes-general'),
		'reinit' => true,
		'wpautop' => false,
		// 'tinymce' => false,
		'editor_height' => 200,
		'editor_css' => '<style>.quicktags-toolbar { display: none; }</style>'
	)
);

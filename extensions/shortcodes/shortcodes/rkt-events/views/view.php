<?php if (!defined('FW')) die('Forbidden');

$title = $atts['title'];
$description = $atts['description'];
$location = $atts['location'];

$time_format = 'h:i A';
if (!empty($atts['time_format'])) {
  $time_format = $atts['time_format'];
}

$date_format = 'jS M Y';
if (!empty($atts['date_format'])) {
  $date_format = $atts['date_format'];
}

$icon = '';
if (!empty($atts['icon_image'])) {
  $icon = $atts['icon_image'];
}

// Split date and time out
$time = date($time_format, strtotime($atts['datetime']));
$date = date($date_format, strtotime($atts['datetime']));

$allowed_args = array(
  //formatting
  'strong' => array(),
  'em'     => array(),
  'b'      => array(),
  'i'      => array(),
  'br'      => array(),
  //links
  'a'     => array(
    'href' => array()
  )
);

$date_time_class = '';

if (!empty($atts['hide_date_time']['choice-1']) && !empty($atts['hide_date_time']['choice-2'])) {
  $date_time_class = 'hide-date-time';
} else if (!empty($atts['hide_date_time']['choice-1'])) {
  $date_time_class = 'hide-date';
} else if (!empty($atts['hide_date_time']['choice-2'])) {
  $date_time_class = 'hide-time';
}
?>

<div class="rkt-event">
  <div class="event-inner">
    <?php if ( $icon )  { ?>
      <span class="event-icon">
        <?php echo wptexturize($icon); ?>
      </span>
    <?php } ?>
    <h3 class="event-title type-heading"><?php echo esc_html($title) ?></h3>
    <div class="heading-divider"></div>
    <p class="event-description"><?php echo wp_kses( $description, $allowed_args ) ?></p>
    <?php if ($location) { ?>
      <p class="event-location"><?php echo wp_kses( $location, $allowed_args ) ?></p>
    <?php } ?>
    <div class="event-info <?php echo sanitize_html_class($date_time_class) ?>">
      <h6 class="event-date info"><?php echo esc_html($date) ?></h6>
      <h6 class="event-time info"><?php echo esc_html($time) ?></h6>
    </div>
  </div>
</div>

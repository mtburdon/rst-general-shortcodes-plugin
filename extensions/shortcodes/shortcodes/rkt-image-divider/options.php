<?php if (!defined('FW')) die('Forbidden');

$options = array(
	'image1' => array(
		'label'   => esc_html__('Image 1', 'rkt-shortcodes-general'),
		'desc'    => esc_html__('Add the first image', 'rkt-shortcodes-general'),
		'type'    => 'upload',
		'images_only' => true
	),
	'image2' => array(
		'label'   => esc_html__('Image 2', 'rkt-shortcodes-general'),
		'desc'    => esc_html__('Add the second image', 'rkt-shortcodes-general'),
		'type'    => 'upload',
		'images_only' => true
	),
	'spacer_background_color' => array(
		'type'  => 'rgba-color-picker',
		'value' => 'rgba(7,0,20,1)',
		'label' => __('Spacer background color', 'rkt-shortcodes-general')
	)
);

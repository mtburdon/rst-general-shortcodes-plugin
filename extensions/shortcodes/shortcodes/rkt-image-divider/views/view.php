<?php if (!defined('FW')) die('Forbidden');

if (empty($atts['image1']) || empty($atts['image2'])) {
  return;
}

$image1 = '';
$image2 = '';
$alt_text1 = '';
$alt_text2 = '';
if (!empty($atts['image1']) && !empty($atts['image2'])) {
	$image1 = fw_resize( $atts['image1']['attachment_id'], 400, 400, true );
	$image2 = fw_resize( $atts['image2']['attachment_id'], 400, 400, true );
	$alt_text1 = get_post_meta($atts['image1']['attachment_id'], '_wp_attachment_image_alt', true);
	$alt_text2 = get_post_meta($atts['image2']['attachment_id'], '_wp_attachment_image_alt', true);
}
?>

<section class="rkt-image-divider">
	<div class="image-divider-section">
		<img src="<?php echo esc_url($image1); ?>" alt="<?php echo esc_html($alt_text1) ?>" />
	</div>

	<div class="image-divider-section spacer" style="background: <?php echo esc_attr($atts['spacer_background_color']) ?>;"></div>

	<div class="image-divider-section">
		<img src="<?php echo esc_url($image2); ?>" alt="<?php echo esc_html($alt_text2) ?>" />
	</div>
</section>

<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
	'page_builder' => array(
		'title'       => esc_html__( 'Image header', 'rkt-shortcodes-general' ),
		'description' => esc_html__( 'Add an image block containing a title', 'rkt-shortcodes-general'),
		'tab'         => esc_html__( 'Rocketship Themes', 'rkt-shortcodes-general' )
	),
);
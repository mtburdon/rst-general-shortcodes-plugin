<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'header_image' => array(
		'type'  => 'upload',
		'label' => esc_html__( 'Choose Image', 'rkt-shortcodes-general' ),
		'desc'  => esc_html__( 'Either upload a new image, or choose an existing image from the media library', 'rkt-shortcodes-general' )
	),
  'header_title' => array(
    'type'  => 'text',
    'label' => esc_html__( 'Title', 'rkt-shortcodes-general' ),
    'desc'  => esc_html__( 'The page title', 'rkt-shortcodes-general' )
  ),
  'title_colour' => array(
    'type'  => 'rgba-color-picker',
    'value' => 'rgba(255, 255, 255, 1)',
    'label' => esc_html__('Title Colour', 'rkt-shortcodes-general'),
    'desc'  => esc_html__('Set the colour and opacity of the title text', 'rkt-shortcodes-general')
  ),
  'header_overlay' => array(
    'type'  => 'rgba-color-picker',
    'value' => 'rgba(0, 0, 0, 0.6)',
    'label' => esc_html__('Background Overlay', 'rkt-shortcodes-general'),
    'desc'  => esc_html__('Set the colour and opacity of the overlay. Set the opacity to 0 to disable.', 'rkt-shortcodes-general')
  )
);


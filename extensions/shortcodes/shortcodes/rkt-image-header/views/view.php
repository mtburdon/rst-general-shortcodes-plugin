<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

/**
 * @var array $atts
 */

if ( empty( $atts['header_image'] ) ) {
	return;
}

$image = $atts['header_image']['url'];
$title = $atts['header_title'];
$title_colour = $atts['title_colour'];
$header_overlay = $atts['header_overlay'];
?>

<div class="rkt-image-header" style="background-image: url(<?php echo esc_url($image) ?>);">
  <div class="header-overlay" style="background: <?php echo esc_attr($header_overlay) ?>;">
    <h1 class="header-title" style="color: <?php echo esc_attr($title_colour) ?>;"><?php echo esc_html($title) ?></h1>
  </div>
</div>
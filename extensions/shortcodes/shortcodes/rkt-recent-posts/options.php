<?php if (!defined('FW')) die('Forbidden');

$options = array(
	'title' => array(
		'label'   => esc_html__('How many to show', 'rkt-shortcodes-general'),
		'type'    => 'select',
		'desc'    => esc_html__('How many recent posts should be displayed?', 'rkt-shortcodes-general'),
		'value'		=> 'choice-3',
    'choices' => array(
      'choice-1' => __('1 post', 'rkt-shortcodes-general'),
      'choice-2' => __('2 posts', 'rkt-shortcodes-general'),
      'choice-3' => __('3 posts', 'rkt-shortcodes-general'),
      'choice-4' => __('4 posts', 'rkt-shortcodes-general'),
      'choice-5' => __('5 posts', 'rkt-shortcodes-general'),
      'choice-6' => __('6 posts', 'rkt-shortcodes-general'),
      'choice-7' => __('7 posts', 'rkt-shortcodes-general'),
      'choice-8' => __('8 posts', 'rkt-shortcodes-general'),
      'choice-9' => __('9 posts', 'rkt-shortcodes-general')
    ),
	)
);

<?php if (!defined('FW')) die('Forbidden');

$args = array(
  'posts_per_page' => 3
);

$query = new WP_Query( $args );
?>

<div class="rkt-recent-posts">
  <?php if ( $query -> have_posts() ) :
		while ( $query -> have_posts() ) : $query -> the_post();
			get_template_part( 'post-formats/content', get_post_format() );
		endwhile;
	else :
		get_template_part( 'post-formats/content', 'none' );
	endif; ?>
</div>

<?php wp_reset_query(); ?>

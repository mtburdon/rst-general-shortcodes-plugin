<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
  'intro' => array(
    'type'  => 'html',
    'label' => esc_html__( ' ', 'rkt-shortcodes-general' ),
    'html'  => '<b>' . esc_html__( 'Set how many items should be shown on screen on various devices:', 'rkt-shortcodes-general' ) . '</b>',
  ),
  'items_xl' => array(
    'type'  => 'slider',
    'label' => esc_html__( 'Displays greater than 1200px', 'rkt-shortcodes-general' ),
    'value' => 4,
    'properties' => array(
      'min' => 1,
      'max' => 10,
      'sep' => 1,
      'grid_snap' => true
    )
  ),
  'items_lg' => array(
    'type'  => 'slider',
    'label' => esc_html__( 'Desktops', 'rkt-shortcodes-general' ),
    'value' => 3,
    'properties' => array(
      'min' => 1,
      'max' => 10,
      'sep' => 1,
      'grid_snap' => true
    )
  ),
  'items_md' => array(
    'type'  => 'slider',
    'label' => esc_html__( 'Small desktops', 'rkt-shortcodes-general' ),
    'value' => 2,
    'properties' => array(
      'min' => 1,
      'max' => 10,
      'sep' => 1,
      'grid_snap' => true
    )
  ),
  'items_sm' => array(
    'type'  => 'slider',
    'label' => esc_html__( 'Tablets', 'rkt-shortcodes-general' ),
    'value' => 2,
    'properties' => array(
      'min' => 1,
      'max' => 10,
      'sep' => 1,
      'grid_snap' => true
    )
  ),
  'items_xs' => array(
    'type'  => 'slider',
    'label' => esc_html__( 'Mobile', 'rkt-shortcodes-general' ),
    'value' => 1,
    'properties' => array(
      'min' => 1,
      'max' => 10,
      'sep' => 1,
      'grid_snap' => true
    )
  ),
  'testimonials' => array(
    'type'  => 'addable-box',
    'label' => esc_html__('Items', 'rkt-shortcodes-general'),
    'desc'  => esc_html__('Add some testimonials', 'rkt-shortcodes-general'),
    'help'  => esc_html__('Add testimonials by clicking the "Add" button and filling out the information. You can add as many as you like.', 'rkt-shortcodes-general'),
    'box-options' => array(
      'name' => array( 'type' => 'text', 'label' => esc_html__('Name', 'rkt-shortcodes-general') ),
      'title' => array( 'type' => 'text', 'label' => esc_html__('Title', 'rkt-shortcodes-general') ),
      'description' => array( 'type' => 'textarea', 'label' => esc_html__('Description', 'rkt-shortcodes-general') ),
      'avatar' => array( 'type' => 'upload', 'images_only' => true, 'label' => esc_html__('Image', 'rkt-shortcodes-general') )
    ),
    'template' => '{{- name }}',
    'box-controls' => array(
      'control-id' => '<small class="dashicons dashicons-id-alt"></small>',
    ),
    'limit' => 0,
    'add-button-text' => esc_html__('Add', 'rkt-shortcodes-general'),
    'sortable' => true
  )
);


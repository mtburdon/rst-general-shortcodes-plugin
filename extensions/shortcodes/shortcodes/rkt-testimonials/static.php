<?php if (!defined('FW')) die('Forbidden');

$uri = plugin_dir_url( __FILE__ );

wp_enqueue_script(
  'fw-owl-slider-js',
  $uri . '/static/js/owl.carousel.min.js',
  array('jquery'),
  '1.3.3',
  true
);
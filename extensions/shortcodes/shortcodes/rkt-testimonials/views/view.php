<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

/**
 * @var array $atts
 */

$testimonials = $atts['testimonials'];

if (!$testimonials) {
  return;
}

// Get a unique ID so we can show multiple sliders
$uid = "rktTestimonialId_" . base_convert(microtime(), 10, 36);

$items_xl = $atts['items_xl'];
$items_lg = $atts['items_lg'];
$items_md = $atts['items_md'];
$items_sm = $atts['items_sm'];
$items_xs = $atts['items_xs'];
?>
<script>
  jQuery(function() {
    jQuery("#<?php echo sanitize_html_class($uid) ?>").owlCarousel({
      items: "<?php echo esc_attr($items_xl); ?>",
      itemsDesktop: [1200, "<?php echo esc_attr($items_lg); ?>"],
      itemsDesktopSmall: [979, "<?php echo esc_attr($items_md); ?>"],
      itemsTablet: [768, "<?php echo esc_attr($items_sm); ?>"],
      itemsMobile: [480, "<?php echo esc_attr($items_xs); ?>"]
    });
  });
</script>

<div id="<?php echo sanitize_html_class($uid) ?>" class="rkt-testimonials owl-carousel">
  <?php
  foreach ($testimonials as &$single) {
    ?>
    <div class="testimonial">
      <div class="testimonial-inner">
        <?php if ($single['avatar']) { ?>
          <img class="avatar" alt="<?php echo esc_html($single['name']); ?>" src="<?php echo esc_url($single['avatar']['url']); ?>">
        <?php } ?>
        <div class="inner-container">
          <h4 class="name"><?php echo esc_html($single['name']); ?></h4>
          <?php if ($single['title']) { ?><span class="title"><?php echo esc_html($single['title']); ?></span><?php } ?>
          <?php if ($single['description']) { ?><p class="description"><?php echo esc_html($single['description']); ?></p><?php } ?>
        </div>
      </div>
    </div>
    <?php
  }
  ?>
</div>
<?php if (!defined('FW')) die('Forbidden');

$options = array(
	'timeline_entries' => array(
	  'type'  => 'addable-box',
	  'label' => esc_html__('Items', 'rkt-shortcodes-general'),
	  'desc'  => esc_html__('Add some entries to your timeline', 'rkt-shortcodes-general'),
	  'help'  => esc_html__('Add entries by clicking the "Add" button and filling out the information. You can add as many as you like.', 'rkt-shortcodes-general'),
	  'box-options' => array(
	    'date' => array( 'label'   => esc_html__('Entry Date', 'rkt-shortcodes-general'), 'desc'    => esc_html__('Select the date of this entry', 'rkt-shortcodes-general'), 'type'    => 'date-picker', 'min-date' => '01-01-1900' ),
	    'title' => array( 'label'   => esc_html__('Title', 'rkt-shortcodes-general'), 'desc'    => esc_html__('Add a title for this entry', 'rkt-shortcodes-general'), 'type'    => 'textarea' ),
	    'description' => array( 'label'   => esc_html__('Entry Description', 'rkt-shortcodes-general'), 'desc'    => esc_html__('Add the details for this entry', 'rkt-shortcodes-general'), 'type'    => 'textarea' ),
	    'image' => array( 'label'   => esc_html__('Image', 'rkt-shortcodes-general'), 'desc'    => esc_html__('Optionally add an image for this entry', 'rkt-shortcodes-general'), 'type'    => 'upload', 'images_only' => true )
	  ),
	  'template' => '{{- title }}',
	  'box-controls' => array(
	    'control-id' => '<small class="dashicons dashicons-id-alt"></small>',
	  ),
	  'limit' => 0,
	  'add-button-text' => esc_html__('Add', 'rkt-shortcodes-general'),
	  'sortable' => true
	)
);
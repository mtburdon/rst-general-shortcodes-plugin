<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

/**
 * @var array $atts
 */

$timeline_entries = $atts['timeline_entries'];

if (!$timeline_entries) {
  return;
}
?>

<div class="rkt-timeline">
	<?php
  foreach ($timeline_entries as &$single) {
    ?>
    <div class="timeline-entry">
    	<div class="entry-inner">
    		<?php if ($single['image']) {
          $image = fw_resize( $single['image']['attachment_id'], 100, 100, true ); ?>
    		  <img class="timeline-image" alt="<?php echo esc_html($single['title']); ?>" src="<?php echo esc_url($image); ?>">
    		<?php } else { ?>
          <div class="timeline-no-image"><span class="inner"></span></div>
        <?php } ?>
    		<h4 class="entry-date"><?php echo esc_html(date('l jS F Y', strtotime($single['date']))) ?></h4>
    		<h3 class="entry-title type-heading"><?php echo esc_html($single['title']) ?></h3>
    		<p class="entry-description"><?php echo esc_html($single['description']) ?></p>
    	</div>
    </div>
   	<?php
	}
  ?>
</div>
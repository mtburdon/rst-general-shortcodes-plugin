<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
	'page_builder' => array(
		'tab'         => esc_html__('Layout Elements', 'rkt-shortcodes-general'),
		'title'       => esc_html__('Section', 'rkt-shortcodes-general'),
		'description' => esc_html__('Add a Section', 'rkt-shortcodes-general'),
		'type'        => 'section' // WARNING: Do not edit this
	)
);
<?php if (!defined('FW')) {
	die('Forbidden');
}

$options = array(
  'section_id' => array(
		'label' => esc_html__('Section ID', 'rkt-shortcodes-general'),
		'desc'  => esc_html__('Give this section a unique ID', 'rkt-shortcodes-general'),
		'help'  => esc_html__('If using the site as a one page layout each section that is linked to from the navigation will require a unique ID. This should match the ID set in the navigation', 'rkt-shortcodes-general'),
		'type'  => 'text',
		'attr'  => array( 'class' => 'code-field' )
	),
	'section_class' => array(
		'label' => esc_html__('Section Class', 'rkt-shortcodes-general'),
		'desc'  => esc_html__('Give this section a class if you wish to add some custom CSS', 'rkt-shortcodes-general'),
		'help'  => esc_html__('If you would like to apply some custom CSS to this section add a class then use it within the Custom CSS tab in the Theme Settings', 'rkt-shortcodes-general'),
		'type'  => 'text',
		'attr'  => array( 'class' => 'code-field' )
	),
	'background_settings' => array(
		'type'         => 'multi-picker',
		'label'        => false,
		'desc'         => false,
		'picker'       => array(
			'background' => array(
				'label'   => esc_html__( 'Background Settings', 'rkt-shortcodes-general' ),
				'desc'    => esc_html__( 'Set up some background settings for this section', 'rkt-shortcodes-general' ),
				'type'    => 'radio',
				'choices' => array(
					'none'    => esc_html__( 'None', 'rkt-shortcodes-general' ),
					'image'   => esc_html__( 'Image', 'rkt-shortcodes-general' ),
					'video'   => esc_html__( 'Video', 'rkt-shortcodes-general' ),
					'colour'   => esc_html__( 'Colour', 'rkt-shortcodes-general' ),
				),
				'value'   => 'none'
			),
		),
		'choices' => array(
			'none'  => array(),
			'image' => array(
				'background_image' => array(
					'label'   => '',
					'type'    => 'background-image',
					'choices' => array(
					)
				),
				'parallax' => array(
					'type'    => 'multi-picker',
					'label'   => false,
					'desc'    => false,
					'picker'  => array(
						'selected' => array(
							'type'         => 'switch',
							'label'        => esc_html__( 'Parallax Effect', 'rkt-shortcodes-general' ),
							'desc'         => esc_html__( 'Create a parallax effect on scroll?', 'rkt-shortcodes-general' ),
							'help'         => esc_html__( 'For best results use an image that is taller than the section\'s height.', 'rkt-shortcodes-general' ),
							'value'        => '',
							'right-choice' => array(
								'value' => 'yes',
								'label' => esc_html__( 'Yes', 'rkt-shortcodes-general' ),
							),
							'left-choice'  => array(
								'value' => 'no',
								'label' => esc_html__( 'No', 'rkt-shortcodes-general' ),
							),
						),
					),
					'choices' => array(
						'yes' => array(
							'parallax_speed' => array(
								'label'      => '',
								'desc'       => esc_html__( 'Set the parallax speed', 'rkt-shortcodes-general' ),
								'type'       => 'slider',
								'value'      => 5,
								'properties' => array(
									'min' => 1,
									'max' => 10,
									'sep' => 1,
									'grid_snap' => true
								),
							),
						)
					)
				)
			),
			'video' => array(
				'video_type'      => array(
					'type'         => 'multi-picker',
					'label'        => false,
					'desc'         => false,
					'picker'       => array(
						'selected' => array(
							'label'   => esc_html__( 'Video Type', 'rkt-shortcodes-general' ),
							'desc'    => esc_html__( 'Select the video type', 'rkt-shortcodes-general' ),
							'type'    => 'radio',
							'choices' => array(
								'youtube'  => esc_html__( 'Youtube', 'rkt-shortcodes-general' ),
								'uploaded' => esc_html__( 'Upload', 'rkt-shortcodes-general' ),
							),
							'value'   => 'youtube'
						),
					),
					'choices'      => array(
						'youtube'  => array(
							'video' => array(
								'label' => '',
								'desc'  => esc_html__( 'Insert a YouTube video URL', 'rkt-shortcodes-general' ),
								'type'  => 'text',
							),
						),
						'uploaded' => array(
							'video' => array(
								'label'       => '',
								'desc'        => esc_html__( 'Upload a video', 'rkt-shortcodes-general' ),
								'images_only' => false,
								'type'        => 'upload',
							),
							'poster' => array(
								'label'   => esc_html__( 'Replacement Image', 'rkt-shortcodes-general' ),
								'type'    => 'background-image',
								'help'    => esc_html__('This image will replace the video on mobile devices that disable background videos', 'rkt-shortcodes-general'),
								'choices' => array(//	in future may will set predefined images
								)
							),
						),
					),
					'show_borders' => false,
				)
			),
			'colour' => array(
				'background_colour' => array(
					'label'   => '',
					'help'    => esc_html__( 'Set a background colour for this section', 'rkt-shortcodes-general' ),
					'desc'    => esc_html__( 'Select the background color', 'rkt-shortcodes-general' ),
					'value'   => '',
					'type'    => 'color-picker'
				),
			),
		),
		'show_borders' => false,
	),
	'is_fullwidth' => array(
		'type'    => 'multi-picker',
		'label'   => false,
		'desc'    => false,
		'picker'  => array(
			'selected' => array(
				'label' => esc_html__( 'Full Width Content', 'rkt-shortcodes-general' ),
				'type'  => 'switch',
				'desc'  => 'Make the content inside this section full width?',
				'left-choice'  => array(
					'value' => 'no',
					'label' => esc_html__( 'No', 'rkt-shortcodes-general' ),
				),
				'right-choice' => array(
					'value' => 'yes',
					'label' => esc_html__( 'Yes', 'rkt-shortcodes-general' ),
				),
				'value'   => 'no',
			),
		),
		'choices' => array(
			'no' => array(
				'container_background_colour' => array(
					'label'   => '',
					'help'    => esc_html__( 'Set the background colour for the sections content', 'rkt-shortcodes-general' ),
					'desc'    => esc_html__( 'Select the content background color', 'rkt-shortcodes-general' ),
					'value'   => '',
					'type'    => 'color-picker'
				),
			),
		),
	),
	'remove_padding' => array(
		'label' => esc_html__('Remove spacing', 'rkt-shortcodes-general'),
		'desc'  => esc_html__('Remove top and bottom padding from this section', 'rkt-shortcodes-general'),
		'type'  => 'switch',
	),
	'overlay_on_picker' => array(
	  'type' => 'multi-picker',
	  'label' => false,
	  'desc' => false,
	  'picker' => array(
	    'overlay_switch' => array(
	      'label' => esc_html__( 'Background overlay?', 'rkt-shortcodes-general' ),
	      'desc' => esc_html__( 'Add a background overlay over the image?', 'rkt-shortcodes-general' ),
	      'type' => 'switch',
	      'right-choice' => array(
	        'value' => 'yes',
	        'label' => esc_html__( 'Yes', 'rkt-shortcodes-general' )
	      ),
	      'left-choice'  => array(
	        'value' => 'no',
	        'label' => esc_html__( 'No', 'rkt-shortcodes-general' )
	      ),
	      'value' => 'no',
	    )
	  ),
	  'choices' => array(
	    'yes' => array(
	      'overlay' => array(
	        'type'  => 'rgba-color-picker',
	        'value' => 'rgba(255, 255, 255, 1)',
	        'label' => esc_html__('Overlay Colour', 'rkt-shortcodes-general'),
	        'desc'  => esc_html__('Set the colour and opacity of the overlay', 'rkt-shortcodes-general')
	      )
	    ),
	    'no' => array(),
	  ),
	  'show_borders' => false,
	),
	'section_overlap' => array(
		'label'   => esc_html__( 'Section Overlap', 'rkt-shortcodes-general' ),
		'desc'    => esc_html__( 'Set a section overlap in px, i.e. 100', 'rkt-shortcodes-general' ),
		'help'    => esc_html__( 'The content that follows will overlap this section with the specified pixel amount.', 'rkt-shortcodes-general' ),
		'type'    => 'short-text',
		'value'   => ''
	),
	'icon_divider' => array(
		'type'  => 'background-image',
		'label' => __('Icon divider', 'rkt-shortcodes-general'),
		'desc'  => __('Add an image as a section divider', 'rkt-shortcodes-general'),
		'choices' => array()
	),
	'icon_background_colour' => array(
		'label' => __('Icon divider background', 'rkt-shortcodes-general'),
		'help'    => esc_html__( 'Set a background colour for the icon divider', 'rkt-shortcodes-general' ),
		'desc'    => esc_html__( 'Select the background color', 'rkt-shortcodes-general' ),
		'value'   => '#ffffff',
		'type'    => 'color-picker'
	)
);

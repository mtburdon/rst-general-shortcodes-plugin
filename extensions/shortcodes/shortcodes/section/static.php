<?php if (!defined('FW')) die('Forbidden');

wp_enqueue_style('fw-ext-builder-frontend-grid');

$uri = plugin_dir_url( __FILE__ );

wp_enqueue_script(
	'fw-shortcode-section-backround-video',
	$uri . '/static/js/min/jquery.fs.wallpaper.min.js',
	array('jquery'),
	false,
	true
);

wp_enqueue_script(
	'fw-shortcode-section',
	$uri . '/static/js/min/scripts.min.js',
	array('fw-shortcode-section-backround-video'),
	false,
	true
);

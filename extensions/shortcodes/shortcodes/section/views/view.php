<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$section_extra_classes = '';
$section_extra_styles = '';

/*
 *	Full width/background colour if not full width
 */
$container_style = '';
$container_class = ( isset( $atts['is_fullwidth']['selected'] ) && $atts['is_fullwidth']['selected'] == 'yes' ) ? 'fw-container-fluid' : 'fw-container';
if (( isset( $atts['is_fullwidth']['selected'] ) && $atts['is_fullwidth']['selected'] == 'no' ) ) {
	$container_style = 'background-color:' . $atts['is_fullwidth']['no']['container_background_colour'] . ';';
}

if ( isset($atts['remove_padding']) && $atts['remove_padding']) {
  $container_style .= ' padding-top: 0; padding-bottom: 0;';
}

$overlay_style = 'none';
if ($atts['overlay_on_picker']['overlay_switch'] === 'yes') {
  $overlay_style = $atts['overlay_on_picker']['yes']['overlay'];
}

/*
 *	Overlap
 */
if ( isset( $atts['section_overlap'] ) && (int)$atts['section_overlap'] > 0 ) {
	$section_extra_styles .= 'margin-bottom:' . - (int)$atts['section_overlap'] . 'px; z-index: 1;';
}

/*
 *	Main section background options
 */
// BG image
$section_parallax = '';
if ( $atts['background_settings']['background'] == 'image' && !empty( $atts['background_settings']['image']['background_image']['data'] ) ) {
	$section_extra_styles .= ' background-image:url(' . $atts['background_settings']['image']['background_image']['data']['icon'] . ');';

	// Parallax
	if ( $atts['background_settings']['image']['parallax']['selected'] == 'yes' ) {
		$section_parallax = 'data-stellar-background-ratio=' . ((int)$atts['background_settings']['image']['parallax']['yes']['parallax_speed'] / 10) . '';
		$section_extra_classes .= ' parallax';
	}
}

// BG colour
if ( $atts['background_settings']['background'] == 'colour' && isset($atts['background_settings']['colour']['background_colour']) && $atts['background_settings']['colour']['background_colour'] != '') {
	$section_extra_styles .= ' background-color: ' . $atts['background_settings']['colour']['background_colour'] . ';';
}

// Video
$bg_video_data_attr = '';
if ( $atts['background_settings']['background'] == 'video' ) {
	$video_poter = '';
	if ( $atts['background_settings']['video']['video_type']['selected'] == 'uploaded' ) {
		$video_url = $atts['background_settings']['video']['video_type']['uploaded']['video']['url'];

		if ( isset( $atts['background_settings']['video']['video_type']['uploaded']['poster']['data']['icon'] ) ) {
			$video_poter = $atts['background_settings']['video']['video_type']['uploaded']['poster']['data']['icon'];
		}
	} else {
		$video_url = $atts['background_settings']['video']['video_type']['youtube']['video'];
	}

	$filetype  = wp_check_filetype( $video_url );
	$filetypes = array( 'mp4' => 'mp4', 'ogv' => 'ogg', 'webm' => 'webm', 'jpg' => 'poster' );
	$filetype  = array_key_exists( (string) $filetype['ext'], $filetypes ) ? $filetypes[ $filetype['ext'] ] : 'video';
	$bg_video_data_attr = json_encode( array( 'source' => array( $filetype => $video_url, 'poster' => $video_poter ) ) );
	$section_extra_classes .= ' background-video';
}

// Icon divider
$icon_divider = '';
if ( !empty( $atts['icon_divider']['data'] )) {
	$icon_divider = $atts['icon_divider']['data']['icon'];
}

$icon_background = 'transparent';
if ( !empty( $atts['icon_background_colour'] )) {
	$icon_background = $atts['icon_background_colour'];
}
?>

<section <?php echo esc_attr($section_parallax); ?> <?php if ($atts['section_id']) { ?>id="<?php echo sanitize_html_class($atts['section_id']); ?>"<?php } ?> class="fw-main-row <?php echo esc_attr($section_extra_classes); ?> <?php echo esc_attr($atts['section_class']); ?>" style="<?php echo esc_attr($section_extra_styles) ?>" data-wallpaper-options="<?php echo fw_htmlspecialchars($bg_video_data_attr) ?>">
	<?php if ( !empty($icon_divider) ) { ?>
		<div class="icon-divider" style="border-color: <?php echo esc_attr($icon_background); ?>;"><img src="<?php echo esc_url( $icon_divider ); ?>"></div>
	<?php } ?>
	<?php if ($atts['overlay_on_picker']['overlay_switch'] === 'yes') { ?>
		<div class="section-overlay" style="background: <?php echo esc_attr($overlay_style) ?>;"></div>
	<?php } ?>
	<div class="<?php echo sanitize_html_class($container_class); ?>" style="<?php echo esc_attr($container_style); ?>">
		<?php echo do_shortcode( $content ); ?>
	</div>
</section>

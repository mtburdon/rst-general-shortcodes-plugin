<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => esc_html__('Special Heading', 'rkt-shortcodes-general'),
	'description'   => esc_html__('Add a Special Heading', 'rkt-shortcodes-general'),
	'tab'           => esc_html__('Rocketship Themes', 'rkt-shortcodes-general'),
);
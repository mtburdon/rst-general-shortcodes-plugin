<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'title'    => array(
		'type'  => 'text',
		'label' => esc_html__( 'Heading Title', 'rkt-shortcodes-general' ),
		'desc'  => esc_html__( 'Write the heading title content', 'rkt-shortcodes-general' ),
	),
	'subtitle' => array(
		'type'  => 'text',
		'label' => esc_html__( 'Heading Subtitle', 'rkt-shortcodes-general' ),
		'desc'  => esc_html__( 'Write the heading subtitle content', 'rkt-shortcodes-general' ),
	),
	'title_text_colour' => array(
	  'type'  => 'rgba-color-picker',
	  'value' => 'rgba(25, 25, 25, 1)',
	  'label' => esc_html__('Heading text Colour', 'rkt-shortcodes-general'),
	  'desc'  => esc_html__('Set the colour and opacity of the heading text', 'rkt-shortcodes-general')
	),
	'subtitle_text_colour' => array(
	  'type'  => 'rgba-color-picker',
	  'value' => 'rgba(119, 119, 119, 1)',
	  'label' => esc_html__('Sub heading text Colour', 'rkt-shortcodes-general'),
	  'desc'  => esc_html__('Set the colour and opacity of the sub heading text', 'rkt-shortcodes-general')
	),
	'heading' => array(
		'type'    => 'select',
		'label'   => esc_html__('Heading Size', 'rkt-shortcodes-general'),
		'choices' => array(
			'h1' => 'H1',
			'h2' => 'H2',
			'h3' => 'H3',
			'h4' => 'H4',
			'h5' => 'H5',
			'h6' => 'H6',
		),
		'value' => 'h2'
	),
	'centered' => array(
		'type'    => 'switch',
		'label'   => esc_html__('Centered', 'rkt-shortcodes-general'),
		'value'		=> true
	)
);

<?php if (!defined('FW')) die( 'Forbidden' );
/**
 * @var $atts
 */
?>
<div class="rkt-heading <?php echo !empty($atts['subtitle']) ? 'sub-heading' : '' ?> <?php echo !empty($atts['centered']) ? 'heading-center' : ''; ?>">
	<?php echo "<{$atts['heading']} class='special-title' style='color: {$atts['title_text_colour']};'>{$atts['title']}</{$atts['heading']}>"; ?>
  <div class="heading-divider" style="color: <?php echo esc_attr($atts['title_text_colour']) ?>;"></div>
	<?php if (!empty($atts['subtitle'])) : ?>
		<span class="special-subtitle" style="color: <?php echo esc_attr($atts['subtitle_text_colour']) ?>;"><?php echo esc_html($atts['subtitle']); ?></span>
	<?php endif; ?>
</div>
<?php
/**
* Plugin Name: Rocketship Themes Shortcodes Collection
* Plugin URI: http://rocketshipthemes.co.uk
* Description: A set of general theme shortcodes to be used with the Unyson framework
* Version: 2.0.2
* Author: Rocketship Themes - Martin Burdon
* Author URI: http://rocketshipthemes.co.uk
*/

add_action('plugins_loaded', 'rkt_shortcodes_theme_load_textdomain');
function rkt_shortcodes_theme_load_textdomain() {
  load_plugin_textdomain( 'rkt-shortcodes-general', false, dirname( plugin_basename(__FILE__) ) . '/language/' );
}

// Overwrite the in-theme location of the shortcodes to use this plugin directory
function rkt_unyson_shortcodes_theme($locations) {
  $locations[dirname(__FILE__) . '/extensions'] = plugin_dir_url( __FILE__ ) . 'extensions';
  return $locations;
}

add_filter('fw_extensions_locations', 'rkt_unyson_shortcodes_theme');

/*
 *  Add custom meta box to pages for header position
 */
// Uncomment following line to show example page meta box
// add_action( 'admin_init', 'rkt_page_settings' );
function rkt_page_settings() {
  add_meta_box(
    'rkt_page_settings',          // HTML ID attribute
    esc_html__( 'Custom Page Settings', 'rkt-shortcodes-general' ),  // Meta box heading
    'rkt_add_options',            // Callback which renders the contents of the meta box
    'page',                       // Custom post type where the meta box should be displayed
    'side',                       // Which part of the screen the meta box should sit
    'high'                        // Priority within the context where the boxes should show
  );
}

// Uncomment next line for example page meta box options
// include 'custom-options/overlay-header.php';

/*
 *  Add all custom options to the meta box
 */
function rkt_add_options( $page ) {
  rkt_add_overlay_header( $page );
}
